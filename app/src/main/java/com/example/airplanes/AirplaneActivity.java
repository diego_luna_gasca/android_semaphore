package com.example.airplanes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class AirplaneActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvRoadOne, tvRoadTwo, tvStatus;
    private ImageView imgSemaphore, imgSemaphore2;
    private boolean intersection = true;
    private int totalRoadOne = 10, totalRoadTwo = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airplane);

        ImageButton btnAddOne = findViewById(R.id.btn_addOne);
        ImageButton btnAddTwo = findViewById(R.id.btn_addTwo);

        imgSemaphore = findViewById(R.id.imgSemaphore1);
        imgSemaphore2 = findViewById(R.id.imgSemaphore2);
        tvRoadOne = findViewById(R.id.tv_roadOne);
        tvRoadTwo = findViewById(R.id.tv_roadTwo);
        tvStatus = findViewById(R.id.tvStatus);

        btnAddOne.setOnClickListener(this);
        btnAddTwo.setOnClickListener(this);

        tvRoadOne.setText(String.valueOf(totalRoadOne));
        tvRoadTwo.setText(String.valueOf(totalRoadTwo));

        Drawable image = getResources().getDrawable(R.drawable.red);
        imgSemaphore.setImageDrawable(image);
        initGreenChangingSemaphore(imgSemaphore2,imgSemaphore);
        startPassingCars();
    }

    private void initGreenChangingSemaphore(final ImageView imgSem, final ImageView imgSem2)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Drawable image = getResources().getDrawable(R.drawable.yellow);
            imgSem.setImageDrawable(image);

        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    Drawable image = getResources().getDrawable(R.drawable.red);
                    imgSem2.setImageDrawable(image);
                    Drawable image2 = getResources().getDrawable(R.drawable.green);
                    imgSem.setImageDrawable(image2);
                }

                initRedChangingSemaphore();
                startPassingCars();
            }
        }, 1000);
    }

    private void initRedChangingSemaphore()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(intersection)
                {
                    intersection = false;
                    initGreenChangingSemaphore(imgSemaphore, imgSemaphore2);
                }
                else {
                    intersection = true;
                    initGreenChangingSemaphore(imgSemaphore2, imgSemaphore);
                }
            }
        }, 6000);
    }

    private void startPassingCars()
    {
        if(intersection)
            recursiveRoadOneActive();
        else
            recursiveRoadTwoActive();
    }

    private void recursiveRoadOneActive()
    {
        if(!intersection)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!intersection)
                    return;

                if(checkStatusForNoCars(tvRoadOne))
                {
                    tvStatus.setText("Intersección 1 sin coches");
                    return;
                }else
                    tvStatus.setText("");

                tvRoadOne.setText(String.valueOf(Integer.parseInt(tvRoadOne.getText().toString()) - 1));
                recursiveRoadOneActive();
            }
        }, 2000);
    }

    private void recursiveRoadTwoActive()
    {
        if(intersection)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(intersection)
                    return;

                if(checkStatusForNoCars(tvRoadTwo))
                {
                    tvStatus.setText("Intersección 2 sin coches");
                    return;
                }else
                    tvStatus.setText("");

                tvRoadTwo.setText(String.valueOf(Integer.parseInt(tvRoadTwo.getText().toString()) - 1));
                recursiveRoadTwoActive();
            }
        }, 2000);
    }

    private boolean checkStatusForNoCars(TextView tvStatus)
    {
        if(Integer.parseInt(tvStatus.getText().toString()) == 0)
            return true;

        return false;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_addOne:
                tvRoadOne.setText(String.valueOf(Integer.parseInt(tvRoadOne.getText().toString()) + 1));
                break;

            case  R.id.btn_addTwo:
                tvRoadTwo.setText(String.valueOf(Integer.parseInt(tvRoadTwo.getText().toString()) + 1));
                break;
        }
    }

    //To detect whether the reource exits in drawable or not
    public static int getDrawableResourceID(Context context,
                                            String identifierName) {

        return context.getResources().getIdentifier(identifierName,
                "drawable", context.getPackageName());
    }
}
